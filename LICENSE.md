## Software License

Copyright 2020 (Rhys Dyson)[https://rhysdyson.com]

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

## Content License

### Jagex and RuneScape Licenses

Jagex ® and RuneScape ® are registered and/or unregistered trade marks of Jagex Limited in the United Kingdom, the United States and other countries. All third party trade marks are the property of their respective owners.

Materials (including without limit all information, software, data, text, photographs, graphics, sound and video) placed on any Jagex Product by them, or on their behalf, are protected by their (or their business partners/suppliers/advertisers or licensors) copyright and other intellectual property rights. You may not use these materials or any Jagex Product except in accordance with these terms and conditions and for personal (i.e. non-commercial) use only.

Read the full Jagex Terms and Conditions found at https://www.jagex.com/en-GB/terms.