/*
  -------------------------------
            GULP FILE
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  THIS FILE SHOULD NOT BE EDITED
  Contact staff regarding changes.
  -------------------------------
  This file manages the gulp command functions. These automate tasks which
  optimise the website. Tasks include compressing images, compiling SCSS/SASS
  to CSS and optimising JavaScript.

/*-------------------------------
        GULP INITIALISATION
-------------------------------*/
const { gulp, src, dest, watch, series, parallel } = require('gulp');

// PACKAGES
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const scsscombine = require('gulp-scss-combine');
const concat = require('gulp-concat');
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const browserSync = require('browser-sync').create();
var clean = require('gulp-clean');
var replace = require('gulp-replace');

// FILE PATHS
// Variables for file input and output locations.
const path = {

  // CODE FILES
  markup: '_developer/**/*.{html,md}', // All markup files to watch
  documentPath: [
    '_developer/_includes/docs/DEVELOPER-GUIDELINES.md',
    '_developer/_includes/docs/README.md',
    '_developer/_includes/docs/PRIVACY-POLICY.md',
    '_developer/_includes/docs/LICENSE.md'
  ],

  cssPath: '_developer/assets/css/**/*.{scss,sass}', // All SCSS files to watch
  cssMain: '_developer/assets/css/main.{scss,sass}', // SCSS @import file to convert into main.css
  cssMainOutput: '_developer/assets/css', // main.css output location

  jsPath: '_developer/assets/js/**/*.js', // All JS files to watch and convert into main.js
  jsMain: '_developer/assets/js/main.js',
  jsOutput: '_developer/assets/js', // main.js output location

  imagePath: '_developer/assets/_images/**/*.*',
  imageInput: '_developer/assets/_images/**/*.{png,jpg,jpeg,ico}',
  imageOutput: '_developer/assets/img'
}

/*-------------------------------
            GULP TASKS
-------------------------------*/

// DEFAULT TASK:
exports.default = series(
  cssTask,
  watchTask
);

exports.bust = series(
  cacheBustTask
);

// CSS TASK:
// Compile SCSS & SASS into main.css, then compress it.
exports.css = cssTask;

function cssTask() {
  console.log(' -------------------- \n COMPILING STYLESHEETS \n --------------------');
  return src([
    path.cssMain
    ])
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(postcss([ autoprefixer(), cssnano() ]))
    .pipe(sourcemaps.write('.'))
    .pipe(dest(path.cssMainOutput))
    .pipe(browserSync.stream()
  );
}

// UPDATE TASKS
// These copy the updated CSS and JS files from the _developer
// directory, and place them into _site for faster page updating.
function updateCSS() {
  console.log(' -------------------- \n DISPLAYING STYLESHEETS \n --------------------');
  return src(path.cssMain)
    .pipe(dest('_site/assets/css')
  );
}

// WATCH TASK
// Boots a local browser server to display the site. Automaticaly regenerates
// when files are changed or updated.
exports.watch = watchTask;

function watchTask(){
  console.log(' -------------------- \n BOOTING LOCAL SERVER \n --------------------');
  browserSync.init({
    server: {
      baseDir: '_site'
    }
  });

  watch(path.documentPath, {delay: 1500, interval: 3000, usePolling: true});
  watch(path.markup,{delay: 1500, interval: 3000, usePolling: true}).on('change', browserSync.reload);
  watch(path.cssPath, {delay: 1500, interval: 3000, usePolling: true}, series(cssTask, updateCSS)).on('change', browserSync.reload);
  // watch(path.imagePath, {delay: 1500, interval: 3000, usePolling: true}, series(imageTask));
}

// IMAGE TASK:
// Compresses image files //  NEEDS UPDATING TO LOOP THROUGH ALL FILES https://github.com/postcss/gulp-postcss
exports.image = series(
  imageClean,
  imageTask
);

function imageClean() {
  console.log(' -------------------- \n CLEANING IMAGES \n --------------------');
  return src(path.imageOutput, {allowEmpty: true})
    .pipe(clean()
    );
}

function imageTask() {
  console.log(' -------------------- \n COMPRESSING IMAGES \n --------------------');
  return src(path.imageInput)
      .pipe(imagemin())
      .pipe(dest(path.imageOutput)
  );
}

var cbString = new Date().getTime();
function cacheBustTask(){
    return src(['index.html'])
        .pipe(replace(/cb=\d+/, 'cb=' + cbString))
        .pipe(dest('.'));
}