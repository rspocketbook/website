---
layout: page
title: Magic Spells
---

## Introduction

Spells are magic projectiles created from the combination and consumption of Runes. Runes may be held and consumed from the inventory or stored in a Rune Pouch which allows them to be equipped in the quiver/ammunition slot.

## Spells

The spells outlined in the table below are a few high level spells that provide great damage output and survivability in multiplayer boss fights.

{% include tables/projectiles/spells.html %}
