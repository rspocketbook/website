---
layout: page
title: Ranged Projectiles
---

## Introduction

Arrows and Bolts are projectiles shot out of ranged weapons. They are stored in the projectiles equipment slot on the player. Ranged ammunition can also be stored within the Tirannwn Quiver to gain a small prayer bonus from the quiver equipment slot.

## Bakriminel Bolts

Bakriminel bolts are high levelled bolts which can be compared to Ancient Spells, as they provide secondary effects ranging increased adrenaline generation to life steal. They are usually paired with the highest tier of dual wield set of crossbows.

## Arrows and Bolts

{% include tables/projectiles/bakriminel-bolts.html %}
