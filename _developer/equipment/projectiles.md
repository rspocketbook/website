---
layout: page
title: Projectiles
---

## Quivers and Pouches

Quivers and pouches store arrows, bolts and runes in the players quiver slot. Quivers provide prayer bonuses and rune pouches store multiple types of runes.

[Quivers & Pouches](/equipment/projectiles/quivers-and-pouches.html){: .button}

## Ranged Projectiles

Ranged projectiles include arrows and bolts, and are fired from bows and crossbows respectively.

[Ranged Projectiles](/equipment/projectiles/ranged.html){: .button}

## Magic Spells

Spells are magic projectiles and are conjured through the use of runes.

[Magic Spells](/equipment/projectiles/magic.html){: .button}