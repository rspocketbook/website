---
layout: page
title: Armour
---

## Power Sets

Power armour sets provide increased damage output rather than health. These sets use invention perks optimised for damage output towards bosses.

[Power Sets](/equipment/armour/power.html){: .button}

## Tank Sets

Tank armour sets provide increased health and defence, and are used by players in the tank role at bosses.

[Tank Sets](/equipment/armour/tank.html){: .button}

## Slayer Sets

Slayer sets are power armour sets equipped with slayer specific perks to help speed up tasks.

[Slayer Sets](/equipment/armour/slayer.html){: .button}

## Shields and Defenders

Shields and defenders are used to cast defensive abilities.

[Shields & Defenders](/equipment/armour/shields-and-defenders.html){: .button}

## Gloves and Boots

Gloves and boots are used to provide armour and strength boosts depending on the players role. They can not be augmented.

[Gloves & Boots](/equipment/armour/gloves-and-boots.html){: .button}

## Specialty Armour

Specialty armour pieces are from achievements and quests and are useful in various specific situations.

[Specialty Armour](/equipment/armour/specialty.html){: .button}