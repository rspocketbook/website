---
layout: page
title: Rings
---

## Introduction

Rings provide a wide range of stat boosts and passive effects. Similar to many weapon switches, rings are often carried into combat for the sake of switching to activate various effects.

## Rings

The table below outlines a number of useful rings. The two most common types of rings are those that affect adrenaline usage and drop rates.

{% include tables/accessories/rings.html %}
