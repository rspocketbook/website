---
layout: page
title: Tool Belt
---

## Introduction

Tool belt items can be added to a players hidden tool belt, where they can provide passive effects and be toggled on or off. Many of these items are unlocked through the Dungeoneering skill, and upgraded through Player Owned Ports, Farms and Treasure Hunter.

## Tool Belt Items

The items listed below will primarily help players by managing creature drops during slayer tasks.

{% include tables/accessories/tool-belt.html %}
