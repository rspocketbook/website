---
layout: page
title: Amulets
---

## Introduction

Amulets provide a wide range of stat boosts and passive effects. These range from increasing the damage or effect of specific abilities and prayers such as Dragon Breath and Soul Split, to adding completely new effects such as the damage stacking of the Reaper Necklace.

## Power Amulets

The neckwear outlined in the table below are some of the most powerful and expensive neckwear items. These items provide powerful stat boosts and effects which are perfect for bossing.

Ornament kits can be gathered from master clue scrolls to boost the stats of a Reaper Necklace or Amulet of Souls.

{% include tables/accessories/amulets/power-amulets.html %}

## Slayer Amulets

The neckwear outlined in the table below are perfect for slayer tasks and players who don't yet have access to the amulets in the power table above.

These amulets provide effects such as restoring prayer, health and increasing damage to monsters, which can make slayer tasks more AFK or faster. Blood Necklaces both deal damage and heal the player.

{% include tables/accessories/amulets/slayer-amulets.html %}
