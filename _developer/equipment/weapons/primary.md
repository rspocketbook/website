---
layout: page
title: Primary Weapons
---

## Introduction

Unlike armour sets, the same weapons are recommended for both bossing and slayer tasks. Therefore, the highest tier weapon and the most powerful damage dealing perks are the best for both tasks. Two-handed weapons are often more cost effective, and suitable for most tasks, particularly for melee combat where 2h's provide AOE damage. Dual wielding on the other hand will often deal more damage to a single target, if the player has a 100% hit chance on the enemy.

## Weapons

The following tables outline the current best weapon sets for all combat styles. These are usually always the highest tier weapons in the game—which are currently level 92. In the event you don’t own and/or cannot afford weapons at this tier — substitute down to the tier you can afford.

{% include tables/weapons/primary.html %}

## Weapon Perks

Weapon perks focus on maximum damage output. It's important to remember to put Aftershock on the main hand of a duel wield set, to keep the effect of the ability active when switching the offhand to a shield or another weapon switch.

{% include tables/weapons/primary-perks.html %}
