---
layout: page
title: Weapon Switches
---

## Introduction

Weapon switches are primarily used to temporarily apply a perk effect, and are often equipped before casting a specific ability.

## Perks

The table below outlines a list of common perks used for switching effects.

{% include tables/weapons/switches-perks.html %}

## Switches

The table below outlines a number of items commonly used as switches, as well as the perks they are used for.

{% include tables/weapons/switches.html %}
