---
layout: page
title: Miscellaneous Equipment
---

## Introduction

This page is a collection of miscellaneous items you might find useful in preparation for or during combat. Most of these items reside in the players' inventory.

## Damage Tools

These items are used to increase DPS in various ways.

{% include tables/equipment/damage-output.html %}

## Teleportation Artefacts

These items help you transverse Gielinor faster.

{% include tables/equipment/teleports.html %}

## Regeneration Items

These items provide passively regenerate health and prayer from the inventory.

{% include tables/equipment/regeneration.html %}