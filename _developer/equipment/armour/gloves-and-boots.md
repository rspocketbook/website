---
layout: page
title: Gloves and Boots
---

## Introduction

Gloves and boots offer additional defensive stats, alongside some secondary passive abilities that can increase damage in various situations. Neither gloves nor boots can be augmented.

## Hand Options

{% include tables/armour/gloves.html %}

## Feet Options

{% include tables/armour/boots.html %}
