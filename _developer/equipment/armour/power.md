---
layout: page
title: PvM Power Armour Sets
---

## Armour Calculator

{% include tools/armour-calculator.html %}

## Introduction

Power armour sets are optimised for offensive use: to maximise damage output to a target or boss. As a result, these sets are made up of the highest tier sets available to increase damage output and maximise a players' kills per hour.

Higher tiered sets are usually recommended exclusively for bosses, while tier 70 armour sets are recommended for slayer tasks. Tier 70 sets provide adequate DPS and defensive bonuses while costing far less to maintain as non-degradable items. Also, they provide a cost effective solution as an additional set which can be equipped with slayer specific perks to speed up slayer tasks. If you wish to view armour sets for slayer, please [click to view the slayer armour sets]({% link equipment/armour/slayer.md %}).

## Power Armour Sets

Power armours provide an increased attack bonus, at the cost of 5 levels worth of defence. For example, tier 90 power armour offers the damage increase of tier 90 equipment, but the defence increase of level 85 gear. Below is a table outlining the various power armour sets ideal for bossing.

{% include tables/armour/power-sets.html %}

## Power Armour Perks

As power armour sets are natually built for damage output, they are further enhanced with damage dealing perks. The perks listed below are the best in slot perks to equip, and can become expensive when trying to obtain the highest ranked perk.

{% include tables/armour/power-perks.html %}

### Note:
- Zamorak Mjolnirs can be collected for free Zamorak components ([Guide](https://www.youtube.com/watch?v=pBmaJYu44lA)).
