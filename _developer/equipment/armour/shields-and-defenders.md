---
layout: page
title: Shields & Defenders
---

## Introduction

The shield slot is primarily used for shield switches to allow the player to cast defensive abilities. Some abilities require the shield to be equipped for the full duration while others simply just need it to be equipped long enough to trigger the ability (such as Resonance). Shields are predominately used in bossing to defend against one-shot kills, or by players in the tank position of group bosses. Defensive abilities significantly reduce incoming damage, or convert the received damage into health.

## Shield Options

As shields are often used momentarily to trigger an defensive ability. The level of a shield greater affects healing effects than damage reduction abilities. For example, a level 90 shield will heal 95% of the damage that otherwise would have been taken when using the Resonance ability. Meanwhile, Devotion will make protection prayers 100% effective and Barricade will make the player immune to all damage, regardless of the shield level, lowering all damage to 1.

Defenders allow a player to use both duel-wield and use defensive abilities, and can therefore provide a more diverse weapon switch alongside single-handed weapons.

{% include tables/armour/shields.html %}

## Shield Perks

Shield perks are designed to further reduce incoming damage, or lower ability cooldowns. They are particularly helpful for tanks. Turtling increases the duration and decreased the cooldown of Barricade.

Spirit shields have a passive ability to reduce incoming damage by 30%, at the cost of prayer points, and can be combined with the Lucky perk to add a small chance to reduce any damage received to 1 point.

Precise is an offensive ability, to increase the minimum hit a player deals, and is best added onto a defender to raise the player's minimum DPS, and help counter the damage loss from using a proper off-hand weapon.

{% include tables/armour/shield-perks.html %}
