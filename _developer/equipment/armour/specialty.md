---
layout: page
title: Specialty Armour
---

## Introduction

Specialty armour pieces are typically tools used for their passive effects and aside from the slayer helmet, are rarely the best in-slot item. Most of these items are rewards from quests, skills or achievement diaries.

## Specialty Armour and their Uses

{% include tables/armour/specialty.html %}
