---
layout: page
title: PvM Tank Armour Sets
---

## Introduction

Tank armour yields a higher defensive stat than power armour. In addition, tank gear often boosts a players' base health. It is specifically used by specialist team members in group raids with the goal of distracting a boss and soaking up damage.

## Tank Armour Sets

As tank armour is especially niche, only the highest level equipment is used. Players unable to obtain this gear should consider the DPS role.

{% include tables/armour/tank-sets.html %}

## Tank Armour Perks

Tank armour perks are similar to the power armour perks, with the substitution of Mobile for Lucky. Lucky gives a small chance for incoming damage to be reduced to 1. Only rank 1 can be obtained alongside Crackling.

{% include tables/armour/tank-perks.html %}
