---
layout: page
title: Weapons
---

## Primary Weapons

Primary weapons are used for dealing maximum damage, equipped with high damage perks.

[Primary Weapons](/equipment/weapons/primary.html){: .button}

## Weapon Switches

Weapon switches are equipped tempoarily to utilise the effects of additional perks.

[Weapon Switches](/equipment/weapons/switches.html){: .button}

## Specialty Weapons

Specialty weapons are from achievements and quests and are useful in various specific situations.

[Specialty Weapons](/equipment/weapons/specialty.html){: .button}