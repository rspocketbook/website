---
layout: page
title: Equipment
---

## Introduction

The equipment section refers to all items which can be worn, equipped or activated by the player to provide a temporary buff or effect. Items such as armour, weapons, projectiles and accessories are stored in the worn equipment interface, while others are stored in menus such as the tool belt or inventory.

## Armour

![Armour Icon](assets/img/game-icon/item-slot/head.png "Head slot")
![Torso Icon](assets/img/game-icon/item-slot/body.png "Torso slot")
![Leg Icon](assets/img/game-icon/item-slot/legs.png "Legs slot")
![Shield Icon](assets/img/game-icon/item-slot/shield.png "Shield slot")
![Feet Icon](assets/img/game-icon/item-slot/feet.png "Feet slot")
![Hands Icon](assets/img/game-icon/item-slot/gloves.png "Hands slot")

The armour section covers all categories of armour from complete sets to shields, defenders, gloves and boots. Power armour sets are broken into two categories, where they are optimised for either PvM (bosing) or Slayer through the use of invention perks. Quest and achievement rewards are outlined in the specialty section and are useful for specific bosses and/or slayer tasks.

[Armour](/equipment/armour.html){: .button}

## Weapons

![Main-hand Icon](assets/img/game-icon/item-slot/main-hand.png "Main-hand Slot")
![Off-hand Icon](assets/img/game-icon/item-slot/off-hand.png "Off-hand Slot")
![Two-hand Icon](assets/img/game-icon/item-slot/two-hand.png "Two-hand Slots")

The weapon section includes all main-hand, off-hand (dual wields) and two-handed weapon options optimised for damage output. It additionally includes weapon switches and specialty weapons which can be useful in niche situations.

[Weapons](/equipment/weapons.html){: .button}

## Projectiles

![Ammo Icon](assets/img/game-icon/item-slot/ammo.png "Ammo Slot")

The projectiles section covers all items which can be equipped in the ammo slot. These include quivers and rune pouches, as well as the arrows, bolts and runes to store within them.

[Projectiles](/equipment/projectiles.html){: .button}

## Accessories

![Neck Icon](assets/img/game-icon/item-slot/neck.png "Neck Slot")
![Ring Icon](assets/img/game-icon/item-slot/ring.png "Ring Slot")
![Cape Icon](assets/img/game-icon/item-slot/cape.png "Cape Slot")
![Aura Icon](assets/img/game-icon/item-slot/aura.png "Aura Slot")
![Pocket Icon](assets/img/game-icon/item-slot/pocket.png "Pocket Slot")
![Tool Belt Icon](assets/img/game-icon/item-slot/tool-belt.png "Tool Belt Slot")

Accessories inclue all passive effect items, including jewelery, capes, auras, pocket slot and tool belt items. Specialty tools include additional tools which can be stored in the inventory such as teleports and damage-increasing tools.

[Accessories](/equipment/accessories.html){: .button}