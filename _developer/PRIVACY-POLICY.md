---
layout: page
title: Privacy Policy
permalink: privacy-policy.html
---

<!--
    The content for the privacy policy is included, so it can also be duplicated into the root
    by gulp, so it appears the same on the repository page.
-->

{% include docs/PRIVACY-POLICY.md %}
