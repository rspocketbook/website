## Development and File Overview

The root directory ([this repository](https://gitlab.com/rspocketbook/website)) contains the various software files for development and deployment. These files control the settings of this GitLab repository, Jekyll and gulp (more on these later). These files should not be modified or changed, and are locked on the repository.

The ([developer directory](https://gitlab.com/rspocketbook/website)) contains all the website files and data. This is the only directory you will need to enter and make changes for the live site. Files will be output in a 'site' directory by Jekyll, which is the directory that goes live on the web server.

## Software

This website has been built with both Ruby and Node.js developer tools. If you don't know much about these, this page will help you get started with them. For those of you that do, this website uses Jekyll (gem) and gulp (node).

### Getting Started with Ruby

Ruby is required to install Ruby applications and software (called Gems), which includes a large list of development tools. Open the terminal, and type "ruby -v" to check if you have Ruby installed. We use Ruby 2.7.0, the latest as of March 2020.

#### Windows Update/Install
Windows users can install/update Ruby with the [Ruby Installer](rubyinstaller.org/).

#### Mac Update/Install
Mac users can install [Homebrew](https://brew.sh/) to manage their version of Ruby.

Run ' /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)" '
To install Ruby: 'brew install ruby'
To update Ruby: 'brew upgrade ruby'
Set 2.7.0 as default: 'rvm install "ruby-2.7.0"'

#### Installing Bundler

[Bundler](https://bundler.io/) is a Ruby Gem which is used to install and maintain other Gems. It stores installed Gems for a project in the 'Gemfile' and ensures we all use the same Gems and versions.

To install bundler, run the command 'gem install bundler'.

Next, run 'bundle install' to install all the RSPocketBook gems. Currently, we are only using Jekyll. Jekyll is a website builder which builds static websites (more on this later).

### Getting Started with Node.js

Node.js is used similar to Ruby in that it's used to install packages. However, it already functions like Bundler, where it stores all the packages in the 'package.json' file. You can install Node from the [Node Website](nodejs.org/).

Run the "npm install --only=dev" command to install the node packages and their versions.

You may need to run "npm audit fix" after this installation.

#### Install Gulp

Gulp is a tool we can use to create file-management commands, to compile our code, compress our code and image files and more. Gulp requires a global install, so it wont install through 'npm install'.

Run 'npm install gulp -g' to install Gulp.
Run 'gulp -v' to confirm the installation.

On Mac, you may may get an EACCES eror-meaning you don't have permission to change the files in the node_modules directory (when installing packages).
To fix this, run 'whoami' to get your full username.
Run 'sudo chown -R USERNAME: /usr/local/lib/node_modules' to grant yourself permission. Your Mac password will need to be entered.
Run 'npm install gulp -g' to install gulp.
Run 'gulp -v' to ensure it's installed.

You can now run our custom gulp commands with 'gulp OURCOMMAND'.

### Editing the Website

To contribute to the website and make edits, you can either edit files/pages live on the repository, or clone the repository. Cloning the repository will require you to install two software tools for local development, Jekyll and Gulp. Both of these are command line tools, and require a small bit of knowledge using the terminal (command prompt).

#### Jekyll

Jekyll is a static website builder that takes all the web files from the developer directory and converts them into a convenient website package. Jekyll runs on the GitLab pages server, where the live site is hosted. It is required to be installed if you wish to test your changes on a local copy of the website (on localhost). Jekyll is a Ruby application, and needs to be installed with Bundler in the terminal.

Getting started with Jekyll:
([Jekyll Website](https://jekyllrb.com/))

Windows users need to remove the '$' from each command.

1. Open terminal or command prompt.
2. Run '$ gem install bundler'.
3. Run '$ gem install bundler jekyll'.
4. Enter the repository:
  'cd repository/rspocketbook/website'
5. Run 'bundle exec jekyll serve' to start the Jekyll server'.
6. Open 'http://localhost:4000' in your browser.

#### Gulp

Gulp is a tool used to create custom terminal commands which can do a bunch of powerful tasks. These include moving and compressing files, compiling SCSS code to CSS, and compiling it to a single file (we use SASS), concatenating (merging) JavaScript code into a single file and more. Gulp is a Node.js application, and therefore requires node to be installed.

Getting started with Gulp:
([Gulp Website](https://gulpjs.com/docs/en/getting-started/quick-start))

1. Open terminal or command prompt.
2. Run '$ npx version' to check if you have Node. If not, [download it here](https://nodejs.org/en/).
3. Run '$ npm install --global gulp-cli'.
4. Enter the repository:
  'cd repository/rspocketbook/website'.
5. Run '$ npm install'.
6. Run our gulp commands.

OUR GULP COMMANDS ARE CURRENTLY IN REVIEW. We are working to improve they way they work for your convenience.

## File Structure

The site is broken down as per a standard Jekyll website.

- DATA: contains .yml files for navigation menus.
- INCLUDES: house any files that can be included/embedded within other pages with % include file/path %. These include data tables and elements which make up pages.
- LAYOUTS: are page templates, called in the heading of a .md page "layout: page".
- ASSETS: contain all images, fonts, stylesheets and scripts. Images in the IMAGE_DB are raw images, and are compressed and output into IMG by Gulp.
- All other directories without an underscore house the pocketbook pages.

### CSS and JS File Management

- 1-SETUP: All initialisations, frameworks and bare bones setups such as fonts.
- 2.BASICS: Small components such as buttons used to make up elements.
- 3-ELEMENTS: Larger elements which make up pages, such as the header and footer.
- 4-PAGES: Code for complete and individual pages.

## Writing in markdown

Not sure on Markdown syntax? Check out the ([Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)) so we can limit the use of HTML in our .md files. Happy editing!