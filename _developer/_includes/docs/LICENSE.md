# RSPocketBook License

**Copyright (c) 2020 RSPocketBook**

By accessing this Software, you agree to have read and accept the terms of the
following License. This License covers all RSPocketBook ("Products") such as the
Website, PDF and Discord Bot. This License may be edited or modified at any time
by the Product Managers and Repository Owners Blair Quinn and Rhys Dyson.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software",
"RSPocketBook", "PocketBook", "Website" or "PDF"), to edit and modify the Software
without restriction, including without limitation of the rights to pull, use,
modify, merge and push to the original source. Persons with copies of the
Software however may not redistribute, sublicense, and/or sell or rehost copies
of the Software without written concent or approval from the Product Managers.
The Software may not be directly shared or redistributed by means outside of
the RSPocketBook GitLab repository found at https://gitlab.com/rspocketbook.

## Contributions

Any additions ("Contrubutions") you make to the Software or Website become a part
of the RSPocketBook, and are therefore bound by the terms of this Lisense. By
uploading, pushing and merging "Content" (images, text, tables, videos, files
and other data and media) to the Software, you agree that the Contributions you
have added are either original works, you have gained permission to redistrubute
the works or Contribute them to the PocketBook, that they are open source, or
that their use in the RSPocketBook fullfills the requirements outlined in the
License of those products and works.

Making Contributions to the Software may allow a person(s) to be considered an
Editor or Creator. Creators do not have the right to remove the content which
they Contributed as it directly becomes a part of the Software, however they
may contact a Product Manager and request their contributions be removed.

The original creators and moderators of this Software reserve the right to approve,
change or reject Contributions that they believe does not serve fit for the Software.
In addition, they reserve the right to adapt and modify the Contribution to meet
the requirements of the Software at their own discression.

RuneScape is a registered and/or unregistered trademark of Jagex Limited ("Jagex").
Use of RuneScape materials, content and assets are subject to the Jagex Terms and
Conditions found at https://www.jagex.com/en-GB/terms.

### Disclaimer

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
