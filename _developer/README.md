---
layout: page
title: Read Me
permalink: readme.html
---

<!--
    The content for the privacy policy is included, so it can also be duplicated into the root
    by gulp, so it appears the same on the repository page.
-->

{% include docs/README.md %}
