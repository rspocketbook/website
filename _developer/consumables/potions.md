---
layout: page
title: Potions
---

## Introduction

Potions are created with the herblore skill and provide various powerful effects. Potions can heal players as well as boost and restore stats such as prayer, summoning and adrenaline.

#### Flasks and Crystal Flasks

- Players with lower herblore levels should convert their potions (4) into flasks (6) to carry extra dosages into combat.
- Combination potions combine and enhance the effects of multiple potions into a single crystal flask (6). High level players who have access to Prifddinas should make combination potions as soon as possible. Combination recipes are discovered in Daemonheim while Dungeoneering and craftable after paying [Lady Meilyr]({% include url/wiki %}/Lady_Meilyr) a fee.

{% include contents/potions.html %}