---
layout: page
title: Combat Potions
---

## Stat Boosting Potions

Combat potions provide a 6-minute single or multi-stat boost to attack, strength, ranged, magic or defence. They boost the players stat by a percentage plus a set level amount, maxing at 17% + 5 for elder overloads. Overloads come in various tiers and boost all combat stats at once.

{% include tables/consumables/potions/combat.html %}

## Overload Combination Potions

The combination potions below are good alternatives to overload salves for both slayer and bossing. They can be more cost effective than salves, and useful to use when not all salve effects are necessary while still saving on inventory space.

{% include tables/consumables/potions/combination.html %}

{% include contents/potions.html %}