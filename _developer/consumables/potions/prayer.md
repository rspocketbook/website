---
layout: page
title: Prayer Potions & Renewals
---

## Introduction

Prayer potions restore the players prayer points relative to the players prayer level. There are two types of prayer restoration: prayer potions, which instantly restore prayer points, and renewal potions, which restore them over time.

## Prayer Potions

Prayer potions instantly restore prayer by a multiplication of the prayer level, plus a set prayer amount. There are currently four tiers of prayer potion strengths restoring; 317, 327, 416 and 565 prayer at level 99 respectively.

{% include tables/consumables/potions/prayer.html %}

## Prayer Renewal Potions

Prayer renewal potions provide a regeneration over time effect, lasting 310 seconds. There are currently two stengths of restores, base restores regenerate 545 while combination potions such as super prayer renewal and salves regenerate up to 595 prayer at level 99.

Alternatives to renewal potions include the [Demon Horn Necklace]({% include url/wiki %}/Demon_horn_necklace) with a [Bonecrusher]({% include url/wiki %}/Bonecrusher), or the [Ancient Elven Ritual Shard]({% include url/wiki %}/Ancient_elven_ritual_shard), an item stored in the inventory which can be activated to restore prayer.

{% include tables/consumables/potions/renewal.html %}

{% include contents/potions.html %}