---
layout: page
title: Health Potions
---

## Introduction

Health potions are restorative potions which restore and/or increase the maximum lifepoints. Unlike food, health potions **do not reduce adrenaline** upon consumption and can be used to maintain high DPS output.

{% include tables/consumables/potions/health.html %}

{% include contents/potions.html %}