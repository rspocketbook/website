---
layout: page
title: Adrenaline Potions
---

## Introduction

Adrenaline potions (often referred to as powerburtst) provide an instant adrenaline boost. They are primarily used after the activation of a damage boosting ultimate such as death's swiftness or sunshine. This is to allow the activation of more thresholds within the effects of the ultimate.

{% include tables/consumables/potions/adrenaline.html %}

{% include contents/potions.html %}