---
layout: page
title: Food and Healing
---

## Introduction

Food is used to heal the players' health. The amount of health an item of food restores is based on the players' constitution level, where the amount of health will diminish if the constitution level is lower than the required level to receive the full heal. When consuming food, players lose adrenaline. As a result, many plays attempt to use Saradomin brew potions to heal, which restores health without costing adrenaline (and therefore damage). Some new foods such as jellyfish will restore health without costing adrenaline, but usually restore less health than better food sources.

#### Additional Health Sources

- Enhanced excalibar, which applies a powerful heal over time.
- Defensive abilities which can help restore or deflect damage a player takes during a fight.

## Food and Effects

{% include tables/consumables/food.html %}
