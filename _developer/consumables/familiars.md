---
layout: page
title: Familiars
---

# Introduction

Familiars are temporary followers crafted and spawned through the summoning skill. The most useful familiars for combat include fighters and beasts of burden, which either provide stat boosts and attack enemies, or carry items respectively.

## scrolls

Some familiars utilise scrolls for special attacks or effects. Scrolls can be stored in the slayer helmet or the inventory. They can be activated by clicking the cast scroll button in the familiars interface, or by right clicking the summoning emblem in the primary ability bar. Casting can also be changed to the primary function of the emblem, and/or dragged onto an ability bar for keybinding.

## Familiars and Scrolls

{% include tables/consumables/familiars.html %}
