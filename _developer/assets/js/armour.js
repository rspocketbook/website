const nonAugmentable = ' This armour cannot be augmented.';
const augmentable = ' This armour can be augmented.';

var melee = {
    92: 'Trimmed Masterwork',
    90: 'Malevolent',
    80: 'Torva',
    70: 'Bandos',
    60: 'Dragon or Orikalkum',
    50: 'Rune',
    40: 'Adamant',
    30: 'Mithril',
    25: 'Black or White',
    20: 'Steel',
    10: 'Iron',
    1:  'Bronze'
};

function getMelee() {

    const level = document.getElementById("playerLevel").value;
    var infoString = 'At level ' + level + ' defence, you should use: ';

    if ( level >= 92 ) {
        document.getElementById("output").innerHTML = infoString + melee["92"] + ' for melee.' + augmentable;
    } else if ( level >= 90 ) {
        document.getElementById("output").innerHTML = infoString + melee["90"] + ' for melee.' + augmentable;
    } else if ( level >= 80 ) {
        document.getElementById("output").innerHTML = infoString +  melee["80"] + ' for melee.' + augmentable;
    } else if ( level >= 70 ) {
        document.getElementById("output").innerHTML = infoString +  melee["70"] + ' for melee.' + augmentable;
    } else if ( level >= 60 ) {
        document.getElementById("output").innerHTML = infoString +  melee["60"] + ' for melee.' + nonAugmentable;
    } else if ( level >= 50 ) {
        document.getElementById("output").innerHTML = infoString +  melee["50"] + ' for melee.' + nonAugmentable;
    } else if ( level >= 40 ) {
        document.getElementById("output").innerHTML = infoString +  melee["40"] + ' for melee.' + nonAugmentable;
    } else if ( level >= 30 ) {
        document.getElementById("output").innerHTML = infoString +  melee["30"] + ' for melee.' + nonAugmentable;
    } else if ( level >= 25 ) {
        document.getElementById("output").innerHTML = infoString +  melee["25"] + ' for melee.' + nonAugmentable;
    } else if ( level >= 20 ) {
        document.getElementById("output").innerHTML = infoString +  melee["20"] + ' for melee.' + nonAugmentable;
    } else if ( level >= 10 ) {
        document.getElementById("output").innerHTML = infoString +  melee["10"] + ' for melee.' + nonAugmentable;
    } else if ( level >= 1 ) {
        document.getElementById("output").innerHTML = infoString +  melee["1"] + ' for melee.' + nonAugmentable;
    }


}

var ranged = {
    92: 'Elite Sirenic',
    90: 'Sirenic',
    80: 'Pernix',
    70: 'Armadyl',
    60: 'Black dragonhide',
    55: 'Red dragonhide',
    50: 'Blue dragonhide',
    40: 'Green dragonhide',
    30: 'Snakeskin',
    25:  'Frog leather',
    20: 'Studded',
    10: 'Hard leather',
    1:  'Leather'
};

function getRanged() {

    const level = document.getElementById("playerLevel").value;
    var infoString = "At level " + level + " ranged and defence, you should use: ";

    if ( level >= 92 ) {
        document.getElementById("output").innerHTML = infoString + ranged[92] + ' for ranged.' + augmentable;
    } else if ( level >= 90 ) {
        document.getElementById("output").innerHTML = infoString + ranged[90] + ' for ranged.' + augmentable;
    } else if ( level >= 80 ) {
        document.getElementById("output").innerHTML = infoString +  ranged[80] + ' for ranged.' + augmentable;
    } else if ( level >= 70 ) {
        document.getElementById("output").innerHTML = infoString +  ranged[70] + ' for ranged.' + augmentable;
    } else if ( level >= 60 ) {
        document.getElementById("output").innerHTML = infoString +  ranged[60] + ' for ranged.' + nonAugmentable;
    } else if ( level >= 55 ) {
        document.getElementById("output").innerHTML = infoString +  ranged[55] + ' for ranged.' + nonAugmentable;
    } else if ( level >= 50 ) {
        document.getElementById("output").innerHTML = infoString +  ranged[70] + ' for ranged.' + nonAugmentable;
    } else if ( level >= 40 ) {
        document.getElementById("output").innerHTML = infoString +  ranged[40] + ' for ranged.' + nonAugmentable;
    } else if ( level >= 30 ) {
        document.getElementById("output").innerHTML = infoString +  ranged[30] + ' for ranged.' + nonAugmentable;
    } else if ( level >= 25 ) {
        document.getElementById("output").innerHTML = infoString +  ranged[25] + ' for ranged.' + nonAugmentable;
    } else if ( level >= 20 ) {
        document.getElementById("output").innerHTML = infoString +  ranged[20] + ' for ranged.' + nonAugmentable;
    } else if ( level >= 10 ) {
        document.getElementById("output").innerHTML = infoString +  ranged[10] + ' for ranged.' + nonAugmentable;
    } else if ( level >= 1 ) {
        document.getElementById("output").innerHTML = infoString +  ranged[1] + ' for ranged.' + nonAugmentable;
    } 
  
}

var magic = {
    92: 'Elite Tectonic',
    90: 'Tectonic',
    80: 'Virtus',
    70: 'Subjugation',
    60: 'Grifolic',
    50: 'Mystic',
    40: 'Splitbark',
    30: 'Batwing',
    20: 'Spider silk',
    10: 'Imphide',
    1:  'Wizard'
};

function getMagic() {

    const level = document.getElementById("playerLevel").value;
    var infoString = "At level " + level + " magic and defence, you should use: ";
    
    if ( level >= 92 ) {
        document.getElementById("output").innerHTML = infoString + magic[92] + ' for magic.' + augmentable;
    } else if ( level >= 90 ) {
        document.getElementById("output").innerHTML = infoString + magic[90] + ' for magic.' + augmentable;
    } else if ( level >= 80 ) {
        document.getElementById("output").innerHTML = infoString +  magic[80] + ' for magic.' + augmentable;
    } else if ( level >= 70 ) {
        document.getElementById("output").innerHTML = infoString +  magic[70] + ' for magic.' + nonAugmentable;
    } else if ( level >= 60 ) {
        document.getElementById("output").innerHTML = infoString +  magic[60] + ' for magic.' + nonAugmentable;
    } else if ( level >= 50 ) {
        document.getElementById("output").innerHTML = infoString +  magic[50] + ' for magic.' + nonAugmentable;
    } else if ( level >= 40 ) {
        document.getElementById("output").innerHTML = infoString +  magic[40] + ' for magic.' + nonAugmentable;
    } else if ( level >= 30 ) {
        document.getElementById("output").innerHTML = infoString +  magic[30] + ' for magic.' + nonAugmentable;
    } else if ( level >= 20 ) {
        document.getElementById("output").innerHTML = infoString +  magic[20] + ' for magic.' + nonAugmentable;
    } else if ( level >= 10 ) {
        document.getElementById("output").innerHTML = infoString +  magic[10] + ' for magic.' + nonAugmentable;
    } else if ( level >= 1 ) {
        document.getElementById("output").innerHTML = infoString +  magic[1] + ' for magic.' + nonAugmentable;
    } 
  
}