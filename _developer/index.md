---
layout: homepage
title: Quick Links
---

## Welcome All!

Welcome to the RS PocketBook—RuneScapes' most comprehensive gearing guide!

We understand that RuneScape has become quite complex in recent years with new skills such as invention, higher tiers of gear and more items than ever. It can become overwhelming for current players, returning and new players alike.

This guide has been developed to help you manage your goals by outlining the best equipment and setups for you to strive for. This guide outlines a lot of the best in slot gear, but also provides cheaper alternatives at points where the sheer cost may outweigh the marginal benefit of the gear or perk.