---
layout: page
title: Update Archive
permalink: archive.html
---

## Update Log Archive

{% for post in site.posts %} <!-- Offset excludes previous post/most recent -->

<h3>{{ post.title }}</h3>
{{ post.excerpt }}
<a href="{{ post.url }}"><button>View entry</button></a>

{% endfor %}