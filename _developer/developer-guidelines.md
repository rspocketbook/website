---
layout: page
title: Developer Guidelines
permalink: developer-guidelines.html
---

<!--
    The content for the developer guidelines is included, so it can also be duplicated into the root
    by gulp, so it appears the same on the repository page.
-->

{% include docs/DEVELOPER-GUIDELINES.md %}
