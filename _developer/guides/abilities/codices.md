---
layout: page
title: Ability Codices
---

## Introduction

There are many powerful abilities that can be unlocked outside of the regular abilities which are unlocked through skill levels. These are commonly unlocked through boss drops and minigames. These abilities are hidden from the ability menus. When unlocked, they are added to the abilities pages, or replace older versions of abilities for the stronger version.

## Mazcab Abilities

Mazcab abilities are the most important abilities to unlock. They are unlocked through the use of a Mazcab Ability Codex, which can be bought from the G.E or obtained by killing Beastmaster Durzag and Yakamaru. One codex is consumed to unlock one ability.

{% include tables/guides/abilities/ability-codices/mazcab.html %}

## Shattered Worlds Abilities

Shattered Worlds abilities are the second most important codex abilities to unlock. They can be obtained weekly through the Shattered Worlds challenges. Each Ability cost's 63,000,000 Shattered Anima, while the Sigil abilities cost 90,000,000 Shattered Anima.

{% include tables/guides/abilities/ability-codices/shattered-worlds.html %}

## Player Crafted Codices

The Sigil abilities below are crafted by the player. Limitless is created by combining 2,000 Vital Sparks. The Slayer abilities are created through level 88 Invention, and require the blueprint(s) to be purchased for 100,000 Dungeoneering Tokens.

For the slayer abilities, they are best used during Sunshine and Death's swiftness for ranged and magic. However, when used in melee they are best used alongside the ZGS special attack or Threshold rotations, as the Berserk ability  quite often reaches the maximum damage cap.

{% include tables/guides/abilities/ability-codices/sigils.html %}

## Codex Ultimatus (Dig Site)

The Codex Ultimatus is obtained after completing the Dig Site quest and speaking to Terry Balando. It can only be claimed if the player has either 75 Attack, Ranged, Magic, or 91 Constitution to use one of the following abilities. All four abilities are unlocked at the same time. Beware, each tendril ability behaves slightly differently, and each should be used with caution. Consider pairing them with Soul Split or the Vampyrism Aura.

{% include tables/guides/abilities/ability-codices/dig-site.html %}

## God Wars Abilities

The below abilities can be obtained as rare drops from General Graardor or Kree'arra in the God Wars Dungeon. Alternatively, they can be unlocked for 3,500 Reward Currency from Anima Islands.

{% include tables/guides/abilities/ability-codices/god-wars.html %}

## Anima Islands Abilities

Anima islands abilities are unlocked through the Anima Islands minigame. This list includes those that are exclusively obtained through the reward shop.

In August 2019, Sigils were removed alongside the player Sigil slot, and turned into regular abilities.

{% include tables/guides/abilities/ability-codices/anima-islands.html %}

## Dragonkin Laboratory Abilities

The Dragonkin Laboratory ability codices upgrade several melee abilities adding additional effects. These codices can only be unlocked by defeating the three various dragon bosses. There are no tradable versions of these codices.

{% include tables/guides/abilities/ability-codices/dragonkin-laboratory.html %}

## Additional Codices

The codices below are low-priority one-off codices which don't belong to any larger sets. They are generally nice to have and obtain where possible, however the abilities listed above are of much higher priority to unlock.

{% include tables/guides/abilities/ability-codices/specialty.html %}
