---
layout: page
title: Stuns and Crowd Control
---

## Introduction

Stuns prevent enemies from attacking. Within this section is also abilities which change the enemies’ targeted player, and those that can help manage larger waves of monsters.

## Abilities Affected by Flanking 3

The Flanking 3 Invention Perk removes the stun effect of the following abilities and significantly increases their damage, making them some of the most powerful abilities. This makes them worth using over channeled/combo attacks, since it is an instant hit rather than a channeled one.

{% include tables/guides/abilities/stuns-and-crowd-control/flanking.html %}

## Damage and Crowd control

These abilities will help deal with large waves of weaker enemies and deal heavy DPS to a single target.

{% include tables/guides/abilities/stuns-and-crowd-control/damage.html %}

## Miscellaneous Abilities

The following abilities are used to change a boss's target and gain additional adrenaline.

{% include tables/guides/abilities/stuns-and-crowd-control/miscellaneous.html %}
