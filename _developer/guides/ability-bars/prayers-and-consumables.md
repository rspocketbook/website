---
layout: page
title: Bar 2 - Prayers and Consumables
---

## Introduction

The abilities listed in the table below are related to the bar 2 keybinds on the keybinds page. This ability bar is manually cast, and not specific to a particular combat style.

{% include tables/guides/ability-bars/prayers-and-consumables.html %}
