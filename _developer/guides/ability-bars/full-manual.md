---
layout: page
title: Bar 1 - Full Manual Ability Bars
---

## Introduction

The abilities listed in the table below are related to the bar 1 keybinds on the keybinds page. These ability bars are optimised for players using full manual (manually casting all abilities).

{% include tables/guides/ability-bars/full_manual.html %}
