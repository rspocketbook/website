---
layout: page
title: Bar 1 - Revolution Ability Bars
---

## Introduction

The abilities listed in the table below are related to the bar 1 keybinds on the keybinds page. These ability bars are optimised for players using revolution (auto casting basic abilities).

{% include tables/guides/ability-bars/revolution.html %}
