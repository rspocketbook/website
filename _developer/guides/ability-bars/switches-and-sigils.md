---
layout: page
title: Bar 4 - Switches and Sigils
---

## Introduction

The abilities listed in the table below are related to the bar 4 keybinds on the keybinds page. This ability bar is manually cast, and not specific to a particular combat style.

{% include tables/guides/ability-bars/switches-and-sigils.html %}
