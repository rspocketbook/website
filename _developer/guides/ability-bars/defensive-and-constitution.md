---
layout: page
title: Bar 3 - Defensive and Constitution
---

## Introduction

The abilities listed in the table below are related to the bar 3 keybinds on the keybinds page. This ability bar is manually cast, and not specific to a particular combat style.

{% include tables/guides/ability-bars/defensive-and-constitution.html %}
