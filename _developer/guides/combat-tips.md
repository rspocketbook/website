---
layout: page
title: Combat Tips
---

## Introduction

This page outlines a number of tips for combat mechanics to help you increase your DPS during manual play. This includes how auto attacks work, 4-tick auto attacking and cancelling abilities and ending with an explanation of affinity.

## Auto Attacks

Non-offensive abilities (i.e anticipation/freedom) can be used to ‘sneak’ an extra auto attack in with dual wield (DW) & two-handed (2H) weapons. This can be achieved by spam-clicking a target while casting a non-offensive ability.

Most non-offensive abilities can allow a single additional auto attack. Freedom however, allows you to get both MH and OH hits; making it the best ability for sneak-auto-attacks.

<strong>NOTE:</strong> Casting a channeled ability before the non-offensive ability will prevent the additional sneak-auto-attack, so ensure to use a single hit ability first.

### Weapons

- 2H works differently to DW. It is required to wait 1 tick (0.6s) between casting abilities to proc a sneak-auto-attack. This is particularly useful for adding extra damage to an ultimate. The combo should look like: ability 1 (or ultimate) > wait 1 tick > ability 2.

- When weapon switching to use a 2H ultimate, you will have to retarget the enemy every time if the weapon isn’t keybound.

#### Magic

- With magic, you can force auto attacks by keybinding your active spell and activating it right before activating another ability. The auto attack will then stack with that ability.

- When a wand and shield is equipped, you can use an auto attack two ticks after the abilities global cooldown (3 ticks or 1.8s). This is most useful in scenarios like P5 Vorago or Telos.

- Shadow tendrils will fire off an auto attack on the same tick losslessly.

- To sneak in an auto attack with detonate, use your active spell, then release detonate on the same tick followed by a second ability.

#### Debuffs

- Debuffs can also be stacked just like autocasted spells, just remember to keybind them.

## 4-tick Auto Attacking

Four tick auto attacking (4TAA) refers to the act of sneaking in an additional auto attack between the use of basic abilities at the cost of a single game tick of delay between cast abilities. This in turn can result in a significant DPS increase if done consistently and properly. However, this requires a complex series of inputs between every ability and is very input heavy. As a result, this method should only be conducted by serious players who wish to challenge themselves to maximise their damage output.

### Macros

We are aware a large portion of players use macros to 4TAA. These macros are a breach of game rules, and we therefore reccommend players to enter inputs manually or not at all to avoid losing their RuneScape accounts. Currently, it is only possible to 4TAA with magic, until a universal auto attack button is introduced to the game. In order to effectively 4TAA you will require a DW weapon set and a 2H weapon of roughly the same tier.

Please refer to the pipeline provided for an explanation into the method behind 4TAA. If you have any questions; we recommend you join the PocketBook’s Discord server and enquire there.

### Action Sequence Pipeline

![image]({{ site.baseurl }}assets/img/infographic/action-sequence-pipeline.png){:style="display: block; margin: 0 auto; width: 600px; height: auto;"}

The auto attack button must be pressed before the second ability, however they must still be activated on the same tick. If you are completing the above set of actions as shown and it does not seem to work; check to see if the first ability you are using is channeled (such as concentrated blast).

## Cancelling Abilities

Canceling abilities refers to the tactic of manually casting a second ability in-order to end or ‘cancel’ the animation of the previous ability. This is primarily used for channeled or delayed abilities to increase the maximum hit splats and DPS output.

The combo should look like: ability 1 > wait X time/ticks > ability 2.

Due to abilities having various delays they need to be canceled at different times. Often, this can be determined by the buffbar cooldown of a channeled ability (the cooldown until the ability ends).

- Fury and concentrated blast should be canceled immediately after activation.
- These abilities should be canceled just before the ability timer reaches 1 second.
  - Assault varies between DW and 2H, so cast the second ability right as the buff timer hits 1 second.
  - Rapid Fire is a bit slower, so cast the second ability right before the buff timer hits 0 seconds.
- Use an ability 1 tick after global cooldown (3 ticks) to cancel snipe.
- Detonate should be held for 3 seconds before releasing, or you won’t be able to stack it with another ability. You can watch the cooldown go from 27 to 26.
- Cancel onslaught and smoke tendrils as soon as you receive the final self-damaging hitsplat.

## Affinity

Affinity is the largest contributor to whether an attack will hit a target or not. Every creature in the game has a base affinity, which acts as its weaknesses dependent on combat style. The higher a creatures’ affinity is; the more likely they will be able to be hit with attacks. Another interesting note is that in a group boss encounter, affinity is not localised to a single player. If you use the ability quake and give your target 2 affinity, everybody who is attacking that target will have their accuracy increased, not just you.

### Applying Affinity

Affinity can be applied in a variety of ways. Most affinity buffs last one minute before they must be reapplied. Affinity will stack if it is from various sources, but will not stack if it's from the same source. For example, two guthix staff special attacks will not combine to create an affinity of four, however a guthix staff special attack and quake will.

### Calculating Affinity

Affinity contributes to hit chance through the formula: Hitchance = Affinity x (Accuracy/Defence). This may seem like a minor change but it can actually be deceptively large.

Let’s make an example a target with 1,924 total armor (the same as Telos). Assuming you have 3,500 accuracy, and the target has a base affinity of 40.

40 x (3500/1924) = 72.76

Thus you would have 72.76% chance to hit the target with any attack. However if we raise the targets affinity to 42 (+2) you would have an 76.4% chance to hit the target and at 44 affinity, base (+4), you would have an 80% chance to hit. A reminder that this applies not only to you, but to anyone attacking that target.

Affinity can massively influence how easy a boss is to hit. As a result, you and your team should be applying affinity as often as you can.
