---
layout: page
title: Software License
permalink: license.html
---

<!--
    The content for the license is included, so it can also be duplicated into the root
    by gulp, so it appears the same on the repository page.
-->

{% include docs/LICENSE.md %}
